# Admin UI

Admin UI is an administration template for Laravel 5.5. It provides admin layout and basic UI elements to build up an administration area (CMS, e-shop, back-office, ...).

Example of an administration interface built with this package:
![Cuatrokb administration area example](https://docs.getcraftable.com/assets/posts-crud.png "Cuatrokb administration area example")

This packages is part of [Cuatrokb](https://github.com/BRACKETS-by-TRIAD/craftable) (`cuatrokb/craftable`) - an administration starter kit for Laravel 5. You should definitely have a look :)

You can find full documentation at https://docs.getcraftable.com/#/admin-ui
