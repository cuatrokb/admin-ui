<footer class="app-footer">
    {{ trans('cuatrokb::admin-ui.footer.copyright', ['year' => date('Y')]) }}
    <span class="float-right">{{ trans('cuatrokb::admin-ui.footer.powered_by') }} <a href="https://cuatrokb.com">CuatroKB</a></span>
</footer>
