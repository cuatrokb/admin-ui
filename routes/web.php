<?php

Route::middleware(['web', 'admin'])->group(function () {
    Route::namespace('Cuatrokb\AdminUI\Http\Controllers')->group(function () {
        Route::post('/admin/wysiwyg-media','WysiwygMediaUploadController@upload')->name('cuatrokb/admin-ui::wysiwyg-upload');
    });
});
